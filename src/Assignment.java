import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment {
    private JLabel toplabel;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane receivedInfo;
    private JButton checkout;
    private JLabel Total;
    private JPanel root;
    private JTextPane textPane1;
    private JLabel Total2;

    public int totalPrice1 = 0;
    public int totalPrice2 = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order" + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + " ! It will be served as soon as possible."
            );

            String currentText = receivedInfo.getText();
            String priceText = textPane1.getText();
            receivedInfo.setText(currentText + food +"\n");
            textPane1.setText(priceText + price + " yen\n");
            totalPrice1 += (price*1.1);
            totalPrice2 += (price*1.08);
            Total2.setText("Total    " + totalPrice2 + " yen");
            Total.setText("Total    " + totalPrice1 + " yen");

        }
    }

    void CheckOut(){
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to checkout ?",
                    "Payment",
                    JOptionPane.YES_NO_OPTION);

            String selectvalues[] = {"For here", "To go"};


            if (confirmation == 0) {
                if(totalPrice1==0) {
                    int option = JOptionPane.showConfirmDialog(null, "No dish selected",
                            "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);

                }
                if(totalPrice1!=0){
                    int select = JOptionPane.showOptionDialog(null,
                            "Is this for here or to go？",
                            "Question",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            selectvalues,
                            selectvalues[0]
                    );
                    if (select == 1) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is " + totalPrice1 + " yen");
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is " + totalPrice2 + " yen");
                    }
                    receivedInfo.setText("");
                    textPane1.setText("");
                    totalPrice1 = 0;
                    totalPrice2 = 0;
                    Total2.setText("Total    " + totalPrice1 + " yen");
                    Total.setText("Total    " + totalPrice2 + " yen");
                }
            }

    }
    public Assignment() {
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger", 130);
            }
        });
        button2.setIcon(new ImageIcon(this.getClass().getResource("food_hamburger.png")

        ));

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cheeseburger", 160);
            }
        });
        button3.setIcon(new ImageIcon(this.getClass().getResource("food_hamburger_cheese.png")
        ));

        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Drink", 100);
            }
        });
        button5.setIcon(new ImageIcon(this.getClass().getResource("drink_coke.png")
        ));

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chicken Nuggets", 200);
            }
        });
        button1.setIcon(new ImageIcon(this.getClass().getResource("food_chicken_nugget.png")
        ));

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("French fries", 150);
            }
        });
        button4.setIcon(new ImageIcon(this.getClass().getResource("food_frenchfry.png")
        ));

        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad", 290);
            }
        });
        button6.setIcon(new ImageIcon(this.getClass().getResource("food_vegetable_sald.png")
        ));

        checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CheckOut();
            }
        });
        checkout.setIcon(new ImageIcon(this.getClass().getResource("cashier_register.png")
        ));
    }


}